//
//  ContainerController.swift
//  testContainer
//
//  Created by zm_iOS on 16/6/15.
//  Copyright © 2016年 zm_iOS. All rights reserved.
//

import UIKit


public class QPContainerController: UIViewController, QPAnimatedTransitioning {
    public var tabNavBarHight: CGFloat = 30         // tabBar height
    public var enablePopBar: Bool = false           // enable popBar
    public var extendViewLayoutToBar: Bool = false  // do not cover tabnav bar?
    public var enableAnimation: Bool = false       //
    public var enableInteractive: Bool = false      //
    public var animationDuration: NSTimeInterval = 0.5   // animation duration
    
    public enum Effect: Int {
        case Slider
        case Pager
    }
    
    public var effect: Effect = .Pager
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    public convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let tag = Int(arc4random()) % Int.max
    private var childItemControllers = [UIViewController]()
    private var tabNavBarContainer = UIView()     // bar
    private var tabNavContentContainer = UIView()    // container
    private var tabNavPopBar = UIView()
    private var tabNavItemBar = UIView()
    private var selectedItemIndex = NSNotFound
    private var lastSelectedItemIndex = NSNotFound
    private var extendBarToScreenOffset: CGFloat = 0  // tabBar offset
    private var containerLoaded = false               // container finish loading?
    private var animatedTransitioner: QPTransitioneAnimater?
}

// MARK: delete
extension QPContainerController {
    public func animatedFromViewStartTransform() -> CGAffineTransform {
        return CGAffineTransformIdentity
    }
    
    public func animatedFromViewEndTransform() -> CGAffineTransform {
        switch effect {
        case .Slider:
            let transform = CGAffineTransformIdentity
            let direction: CGFloat = selectedItemIndex > lastSelectedItemIndex ? -tabNavContentContainer.bounds.width : tabNavContentContainer.bounds.width
            return CGAffineTransformTranslate(transform, direction, 0)
            
        case .Pager:
            return CGAffineTransformIdentity
        }
    }
    
    public func animatedFromViewFinishTransform() -> CGAffineTransform {
        return CGAffineTransformIdentity
    }
    
    public func animatedToViewStartTransform() -> CGAffineTransform {
        let transform = CGAffineTransformIdentity
        let direction: CGFloat = selectedItemIndex > lastSelectedItemIndex ? tabNavContentContainer.bounds.width : -tabNavContentContainer.bounds.width
        return CGAffineTransformTranslate(transform, direction, 0)
    }
    
    public func animatedToViewEndTransform() -> CGAffineTransform {
        return CGAffineTransformIdentity
    }
    
    public func animatedToViewFinishTransform() -> CGAffineTransform {
        return CGAffineTransformIdentity
    }
    
    public func transitionEnd(complete: Bool) {
        animatedTransitioner = nil
        if complete {
            lastSelectedItemIndex = selectedItemIndex
            
        } else {
            selectedItemIndex = lastSelectedItemIndex
        }
    }
}


// MARK: Private and Override method.

extension QPContainerController {
    /*
      Override list.
     */
    
    public override func loadView() {
        super.loadView()
        /* can`t be seen */
        self.view.backgroundColor = UIColor.clearColor()
        self.view.opaque = true
        
        /* root constraint */
        // TODO: auto constraint
        
        // set frame
        tabNavBarContainer.frame = CGRectMake(0, 0, self.view.bounds.width, extendBarToScreenOffset + tabNavBarHight)
        tabNavItemBar.frame = CGRectMake(0, extendBarToScreenOffset, tabNavBarContainer.bounds.width, tabNavBarHight)
        tabNavContentContainer.frame = CGRectMake(0, (extendViewLayoutToBar ? 0 : tabNavBarContainer.frame.height), self.view.frame.width, self.view.frame.height - (extendViewLayoutToBar ? 0 : tabNavBarContainer.frame.height))
        
        // item container
        tabNavContentContainer.translatesAutoresizingMaskIntoConstraints = false
        tabNavContentContainer.layer.masksToBounds = true
        self.view.addSubview(tabNavContentContainer)
        
        // bar
        tabNavBarContainer.translatesAutoresizingMaskIntoConstraints = false
        tabNavBarContainer.addSubview(tabNavItemBar)
        self.view.addSubview(tabNavBarContainer)
        
        // set flag
        setContainerLoaded()
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
        if childItemControllers.count > 0 {
            customTabNavBarContainer()
            
        } else {
            selectedItemIndex = NSNotFound
        }
    }
    
    public override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if childItemControllers.count > 0 {
            if NSNotFound == selectedItemIndex {
                selectedIndex = 0
                
            } else {
                selectedIndex = selectedItemIndex
            }
        }
        
        // Effect
        
        
        let blurLayer = UIView()
        blurLayer.frame = tabNavBarContainer.bounds
        let blurEffect = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
        blurLayer.addSubview(blurEffect)
        tabNavBarContainer.insertSubview(blurLayer, atIndex: 0)
    }
    
    /*
      Set finish loading flag.
     */
    private func setContainerLoaded() {
        containerLoaded = true
    }
    
    /*
      Loading stat.
     */
    private func isContainerLoaded() -> Bool {
        return containerLoaded
    }
    
    /*
      Load subViewController from one to another.
     */
    private func presentItemController(from: UIViewController?, toItem to: UIViewController) {
        if !to.isViewLoaded() {
            to.loadView()
            to.view.frame = tabNavContentContainer.bounds
            to.view.translatesAutoresizingMaskIntoConstraints = true
            to.view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
            to.viewDidLoad()
            
        }
        
        if nil == from {
            self.addChildViewController(to)
            self.tabNavContentContainer.addSubview(to.view)
            self.didMoveToParentViewController(self)
            lastSelectedItemIndex = selectedItemIndex
            return
        }
        
        if from == to {
            return
        }
        
        animatedTransitioner = QPTransitioneAnimater(containerViewController: self, containerView: tabNavContentContainer, fromViewController: from!, toViewController: to)
        
        if nil != animatedTransitioner {
            animatedTransitioner!.animationDuration = animationDuration
            animatedTransitioner!.shouldAnimated = enableAnimation
            animatedTransitioner!.shouldInteractive = enableInteractive
            animatedTransitioner!.animatedTransitioning = self
            animatedTransitioner!.animating()
        }
    }
    
    /*
      Load viewController at index.
     */
    private func presentItemControllerWithIndex(toIndex to: Int) {
        if childItemControllers.count <= to || to < 0 {
            return
        }
        
        selectedItemIndex = to
        if !isContainerLoaded() {
            return
        }
        
        var fromController: UIViewController?
        if NSNotFound != lastSelectedItemIndex {
            fromController = childItemControllers[lastSelectedItemIndex]
        }
        
        presentItemController(fromController, toItem: childItemControllers[to])
    }
    
    /*
      Custom tabNavBar button items.
     */
    private func customTabNavBarContainer() {
        let itemWidth = tabNavItemBar.frame.width / CGFloat(childItemControllers.count > 0 ? childItemControllers.count : 1)
        
        for (index, item) in childItemControllers.enumerate() {
            let button = UIButton(type: .Custom)
            button.tag = self.tag + index
            button.frame = CGRectMake(CGFloat(index) * itemWidth, 0, itemWidth, tabNavItemBar.bounds.height)
            button.setTitle(item.title, forState: .Normal)
            button.setTitleColor(UIColor.blueColor(), forState: .Normal)
            button.addTarget(self, action: #selector(tabNavItemSelected), forControlEvents: .TouchUpInside)
            tabNavItemBar.addSubview(button)
        }
    }
    
    /*
      Button action.
     */
    func tabNavItemSelected(sender: UIButton) {
        selectedIndex = sender.tag - tag
    }
}

// MARK: Public method.

public extension QPContainerController {
    /**
      Should extend layout to statuBar?
     */
    public var extendBarLayoutToScreen: Bool {
        get {
            return 0 == extendBarToScreenOffset ? false : true
        }
        
        set {
            if newValue {
                extendBarToScreenOffset = UIApplication.sharedApplication().statusBarFrame.height
                
            } else {
                extendBarToScreenOffset = 0
            }
        }
    }
    
    /**
      current selected index
     */
    public var selectedIndex: Int {
        set {
            presentItemControllerWithIndex(toIndex: newValue)
        }
        
        get {
            return selectedItemIndex
        }
    }
    
    public func addItemViewController(childController: UIViewController) {
        childItemControllers.append(childController)
    }
    
    /**
      Add this tabNavBar to super view controller.
     */
    public func addToParentController(viewController: UIViewController) {
        viewController.addChildViewController(self)
        viewController.view.addSubview(self.view)
        self.didMoveToParentViewController(viewController)
        self.parentViewController!.automaticallyAdjustsScrollViewInsets = false
    }
    
    /**
      Remove this tabNavBar from super view controller.
     */
    public func removeFromParentController() {
        if nil != self.parentViewController {
            self.willMoveToParentViewController(nil)
            self.view.removeFromSuperview()
            self.removeFromParentController()
        }
    }
    
}
