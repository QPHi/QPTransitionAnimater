//
//  ViewController.swift
//  testContainer
//
//  Created by zm_iOS on 16/6/15.
//  Copyright © 2016年 zm_iOS. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let container = QPContainerController()
        
        for i in 0...4 {
            let child = ChildController()
            child.title = "child \(i)"
            container.addItemViewController(child)
        }
        
        container.selectedIndex = 3
        container.extendBarLayoutToScreen = true
        container.extendViewLayoutToBar = true
        container.tabNavBarHight = 20
        container.enableAnimation = true
        container.animationDuration = 0.2
        container.addToParentController(self)
    }
}
