//
//  QPAnimatedTransitioner.swift
//  testContainer
//
//  Created by zm_iOS on 16/6/17.
//  Copyright © 2016年 zm_iOS. All rights reserved.
//

import UIKit


public class QPTransitioneAnimater: NSObject {
    public weak var animatedTransitioning: QPAnimatedTransitioning? {
        get {
            return transitionContext.animatedTransitioning
        }
        
        set {
            transitionContext.animatedTransitioning = newValue
        }
    }
    
    public var animationDuration: NSTimeInterval {
        get {
            return transitionContext.animationDuration
        }
        
        set {
            transitionContext.animationDuration = newValue
        }
    }
    
    public var shouldAnimated: Bool {
        get {
            return transitionContext.shouldAnimated
        }
        
        set {
            transitionContext.shouldAnimated = newValue
        }
    }
    
    public var shouldInteractive: Bool {
        get {
            return transitionContext.shouldInteractive
        }
        
        set {
            transitionContext.shouldInteractive = newValue
        }
    }
    
    public init?(containerViewController: UIViewController, containerView: UIView, fromViewController: UIViewController, toViewController: UIViewController) {
        transitionContext = QPTransitionAnimaterContext(containerViewController: containerViewController, containerView: containerView, fromViewController: fromViewController, toViewController: toViewController)
    }
    
    public func animating() {
        transitionContext.startTransition()
    }
    
    private let transitionContext: QPTransitionAnimaterContext
}

