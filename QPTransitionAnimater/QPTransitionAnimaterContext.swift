//
//  QPAnimatedTransitioner.swift
//  testContainer
//
//  Created by zm_iOS on 16/6/17.
//  Copyright © 2016 zm_iOS. All rights reserved.
//

import UIKit


@objc
public protocol QPAnimatedTransitioning  {
    
    /**
     The stat of fromView when the transition begin.
     */
    func animatedFromViewStartTransform() -> CGAffineTransform
    
    /**
     The stat of fromView when the transition end.
     */
    func animatedFromViewEndTransform() -> CGAffineTransform
    
    /**
     The stat of fromView when the transition finish.
     */
    func animatedFromViewFinishTransform() -> CGAffineTransform
    
    /**
     The stat of toView when the transition begin.
     */
    func animatedToViewStartTransform() -> CGAffineTransform
    
    /**
     The stat of toView when the transition end.
     */
    func animatedToViewEndTransform() -> CGAffineTransform
    
    /**
     The stat of toView when the transition finish.
     */
    func animatedToViewFinishTransform() -> CGAffineTransform
    
    /**
     The stat of containerView when the transition begin.
     */
    optional func animatedContainerViewStartTransform() -> CGAffineTransform
    
    /**
     The stat of containerView when the transition end.
     */
    optional func animatedContainerViewEndTransform() -> CGAffineTransform
    
    /**
     The stat of containerView when the transition finish.
     */
    optional func animatedContainerViewFinishTransform() -> CGAffineTransform

    /**
     Is model transition?
     */
    optional func isModeltransition() -> Bool
    
    /**
     When the transition ended.
     */
    optional func transitionEnd(complete: Bool)
}


@available(iOS 7.0, *)
public class QPTransitionAnimaterContext: NSObject, UIViewControllerContextTransitioning {
    
    public weak var animatedTransitioning: QPAnimatedTransitioning?
    public var animationDuration: NSTimeInterval = 0.5
    public var shouldAnimated: Bool = false
    public var shouldInteractive: Bool = false
    
    public init(containerViewController: UIViewController?, containerView: UIView, fromViewController: UIViewController, toViewController: UIViewController) {
        transitionContainerViewController = containerViewController
        transitionContainerView = containerView
        transitionFromViewController = fromViewController
        transitionFromView = transitionFromViewController!.view
        transitionToViewController = toViewController
        transitionToView = transitionToViewController!.view
        super.init()
    }
    
    /*
    public init(containerViewController: UIViewController, containerView: UIView, fromView: UIView, toView: UIView) {
        transitionContainerViewController = containerViewController
        transitionContainerView = containerView
        transitionFromView = fromView
        transitionToView = toView
        super.init()
    }
    */
    
    // MARK: Private
    private unowned let transitionFromView: UIView
    private unowned let transitionToView: UIView
    private weak var transitionContainerView: UIView?      // container view
    private weak var transitionContainerViewController: UIViewController?  // container view controller
    private weak var transitionFromViewController: UIViewController?  // from view controller
    private weak var transitionToViewController: UIViewController?  // to view controller
    private var isAnimating: Bool = false
    private var isCancelled: Bool = false
    private var isInteractiving: Bool = false
}

// MARK: AnimatedTransitioning
extension QPTransitionAnimaterContext {
    
    // This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
    private func animateTransition() {
        transitionFromView.transform = animatedTransitioning?.animatedFromViewStartTransform() ?? CGAffineTransformIdentity
        transitionToView.transform = animatedTransitioning?.animatedToViewStartTransform() ?? CGAffineTransformIdentity
        if nil != animatedTransitioning?.animatedContainerViewStartTransform {
            transitionContainerView?.transform = animatedTransitioning!.animatedContainerViewStartTransform!()
        }
        
        UIView.animateWithDuration(animationDuration, animations: {
            /*[unowned self] in*/
            self.transitionFromView.transform = self.animatedTransitioning?.animatedFromViewEndTransform() ?? CGAffineTransformIdentity
            self.transitionToView.transform = self.animatedTransitioning?.animatedToViewEndTransform() ?? CGAffineTransformIdentity
            if nil != self.animatedTransitioning?.animatedContainerViewEndTransform {
                self.transitionContainerView?.transform = self.animatedTransitioning!.animatedContainerViewEndTransform!()
            }
            
            }, completion: {
                /*[unowned self]*/ complete in
                self.transitionFromView.transform = self.animatedTransitioning?.animatedFromViewFinishTransform() ?? CGAffineTransformIdentity
                self.transitionToView.transform = self.animatedTransitioning?.animatedToViewFinishTransform() ?? CGAffineTransformIdentity
                if nil != self.animatedTransitioning?.animatedContainerViewFinishTransform {
                    self.transitionContainerView?.transform = self.animatedTransitioning!.animatedContainerViewFinishTransform!()
                }
                self.completeTransition(!self.transitionWasCancelled())
        })
        
    }
    
    // This is a convenience and if implemented will be invoked by the system when the transition context's completeTransition: method is invoked.
    private func animationEnded(transitionCompleted: Bool) {
        animatedTransitioning?.transitionEnd?(transitionCompleted)
    }
}

// MARK: ContextTransitioning delegate
extension QPTransitionAnimaterContext {

    public func startTransition() {
        isInteractiving = shouldAnimated
        isCancelled = false
        
        if nil != transitionContainerViewController && nil != transitionToViewController && nil != transitionFromViewController {
            transitionContainerViewController!.addChildViewController(transitionToViewController!)
        }
        
        // TODO:
        transitionContainerView!.addSubview(transitionToView)
        
        if shouldAnimated {
            animateTransition()
            
        } else {
            completeTransition(!transitionWasCancelled())
        }
    }
    
    // This must be called whenever a transition completes (or is cancelled.)
    // Typically this is called by the object conforming to the
    // UIViewControllerAnimatedTransitioning protocol that was vended by the transitioning
    // delegate.  For purely interactive transitions it should be called by the
    // interaction controller. This method effectively updates internal view
    // controller state at the end of the transition.
    public func completeTransition(didComplete: Bool) {
        if nil != transitionContainerViewController && nil != transitionToViewController && nil != transitionFromViewController {
            transitionToViewController!.didMoveToParentViewController(transitionContainerViewController!)
        }
        
        if didComplete {
            // TODO:
            transitionFromView.removeFromSuperview()
            
            if nil != transitionContainerViewController && nil != transitionToViewController && nil != transitionFromViewController {
                // remove fromViewController and add toViewController
                transitionFromViewController!.willMoveToParentViewController(nil)
                transitionFromViewController!.removeFromParentViewController()
            }
            
        } else {
            // TODO: 
            transitionToView.removeFromSuperview()
            
            if nil != transitionContainerViewController && nil != transitionToViewController && nil != transitionFromViewController {
                // add and remove toViewController
                transitionToViewController!.willMoveToParentViewController(nil)
                transitionToViewController!.removeFromParentViewController()
            }
        }
        
        animationEnded(!isCancelled)
    }
    
    
    // Most of the time this is YES. For custom transitions that use the new UIModalPresentationCustom
    // presentation type we will invoke the animateTransition: even though the transition should not be
    // animated. This allows the custom transition to add or remove subviews to the container view.
    public func isAnimated() -> Bool {
        return shouldAnimated
    }
    
    public func isInteractive() -> Bool { // This indicates whether the transition is currently interactive.
        return isInteractiving
    }
    
    public func transitionWasCancelled() -> Bool {
        return isCancelled
    }
    
    // The view in which the animated transition should take place.
    public func containerView() -> UIView? {
        return transitionContainerView
    }
    
    public func fromView() -> UIView {
        return transitionFromView
    }
    
    public func toView() -> UIView {
        return transitionToView
    }
    
    public func presentationStyle() -> UIModalPresentationStyle {
        return UIModalPresentationStyle.Custom
    }
    
    // Currently only two keys are defined by the
    // system - UITransitionContextToViewControllerKey, and
    // UITransitionContextFromViewControllerKey.
    // Animators should not directly manipulate a view controller's views and should
    // use viewForKey: to get views instead.
    public func viewControllerForKey(key: String) -> UIViewController? {
        switch key {
        case UITransitionContextFromViewControllerKey:
            return transitionFromViewController
        case UITransitionContextToViewControllerKey:
            return transitionToViewController
        default:
            return nil
        }
    }
    
    // Currently only two keys are defined by the system -
    // UITransitionContextFromViewKey, and UITransitionContextToViewKey
    // viewForKey: may return nil which would indicate that the animator should not
    // manipulate the associated view controller's view.
    @available(iOS 8.0, *)
    public func viewForKey(key: String) -> UIView? {
        switch key {
        case UITransitionContextFromViewKey:
            return fromView()
        case UITransitionContextToViewKey:
            return toView()
        default:
            return nil
        }
    }
    
    @available(iOS 8.0, *)
    public func targetTransform() -> CGAffineTransform {
        return CGAffineTransformIdentity
    }
    
    // The frame's are set to CGRectZero when they are not known or
    // otherwise undefined.  For example the finalFrame of the
    // fromViewController will be CGRectZero if and only if the fromView will be
    // removed from the window at the end of the transition. On the other
    // hand, if the finalFrame is not CGRectZero then it must be respected
    // at the end of the transition.
    public func initialFrameForViewController(vc: UIViewController) -> CGRect {
        return CGRectZero
    }
    
    public func finalFrameForViewController(vc: UIViewController) -> CGRect {
        return vc.view.frame
    }
    
    // MARK: Interactive Part
    
    // It only makes sense to call these from an interaction controller that
    // conforms to the UIViewControllerInteractiveTransitioning protocol and was
    // vended to the system by a container view controller's delegate or, in the case
    // of a present or dismiss, the transitioningDelegate.
    public func updateInteractiveTransition(percentComplete: CGFloat) {
        // TODO: updateInteractiveTransition
    }
    
    public func finishInteractiveTransition() {
        isInteractiving = false
        // TODO: finishInteractiveTransition
    }
    
    public func cancelInteractiveTransition() {
        isInteractiving = false
        isCancelled = true
        // TODO: cancelInteractiveTransition
    }
}
