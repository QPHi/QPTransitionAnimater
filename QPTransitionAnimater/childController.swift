//
//  childController.swift
//  testContainer
//
//  Created by zm_iOS on 16/6/15.
//  Copyright © 2016年 zm_iOS. All rights reserved.
//

import UIKit


class ChildController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let scroll = UIScrollView(frame: CGRectMake(0, 0, self.view.bounds.width, self.view.bounds.height))
//        self.view = scroll
//        scroll.contentSize = CGSizeMake(self.view.bounds.width, self.view.bounds.height)
        let r: CGFloat = CGFloat(arc4random_uniform(256)) / 255
        let g: CGFloat = CGFloat(arc4random_uniform(256)) / 255
        let b: CGFloat = CGFloat(arc4random_uniform(256)) / 255
        self.view.backgroundColor = UIColor(red: r, green: g, blue: b, alpha: 1)
        
        
        if self.title == "child 0" {
            if let path = NSBundle.mainBundle().pathForResource("tayler", ofType: "jpg") {
                 let imageView = UIImageView(frame: self.view.bounds)
                imageView.image = UIImage(contentsOfFile: path)
                self.view.insertSubview(imageView, atIndex: 0)
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
}
